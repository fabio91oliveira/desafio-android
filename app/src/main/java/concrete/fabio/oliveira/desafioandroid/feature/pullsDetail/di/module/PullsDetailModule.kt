package concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.di.module

import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.interactor.PullsDetailInteractor
import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.presenter.PullsDetailPresenter
import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.presenter.impl.PullsDetailPresenterImpl
import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.ui.activity.PullsDetailView
import dagger.Module
import dagger.Provides

/**
 * Created by Fabio Oliveira
 * Email: fabio91oliveira@gmail.com
 * Mobile: +55 (21) 98191-4951
 * LinkedIn: https://www.linkedin.com/in/fabio91oliveira
 */

@Module
class PullsDetailModule(private val view: PullsDetailView) {
    @Provides
    fun provideView() : PullsDetailView {
        return this.view
    }
    @Provides
    fun providePresenter(interactor: PullsDetailInteractor) : PullsDetailPresenter {
        return PullsDetailPresenterImpl(view, interactor)
    }
}