package concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.di.component

import com.google.gson.Gson
import concrete.fabio.oliveira.desafioandroid.feature.di.module.InteractorModule
import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.di.module.PullsDetailModule
import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.interactor.PullsDetailInteractor
import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.ui.activity.PullsDetailActivity
import concrete.fabio.oliveira.desafioandroid.network.di.module.ApiModule
import dagger.Component

/**
 * Created by Fabio Oliveira
 * Email: fabio91oliveira@gmail.com
 * Mobile: +55 (21) 98191-4951
 * LinkedIn: https://www.linkedin.com/in/fabio91oliveira
 */

@Component(modules = [(PullsDetailModule::class), (ApiModule::class), (InteractorModule::class)])
interface PullsDetailComponent {
    fun gson(): Gson
    fun pullsDetailInteractor(): PullsDetailInteractor
    fun inject(activity: PullsDetailActivity)
}