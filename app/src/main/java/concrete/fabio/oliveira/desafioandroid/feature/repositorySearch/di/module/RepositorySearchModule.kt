package concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.di.module

import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.interactor.RepositorySearchInteractor
import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.presenter.RepositorySearchPresenter
import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.presenter.impl.RepositorySearchPresenterImpl
import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.ui.activity.RepositorySearchView
import concrete.fabio.oliveira.desafioandroid.network.Api
import dagger.Module
import dagger.Provides

/**
 * Created by Fabio Oliveira
 * Email: fabio91oliveira@gmail.com
 * Mobile: +55 (21) 98191-4951
 * LinkedIn: https://www.linkedin.com/in/fabio91oliveira
 */

@Module
class RepositorySearchModule(private val view: RepositorySearchView) {
    @Provides
    fun provideView() : RepositorySearchView {
        return this.view
    }
    @Provides
    fun providePresenter(interactor: RepositorySearchInteractor) : RepositorySearchPresenter {
        return RepositorySearchPresenterImpl(view, interactor)
    }
}