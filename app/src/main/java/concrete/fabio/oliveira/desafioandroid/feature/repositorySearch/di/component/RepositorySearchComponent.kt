package concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.di.component

import com.google.gson.Gson
import concrete.fabio.oliveira.desafioandroid.feature.di.module.InteractorModule
import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.di.module.RepositorySearchModule
import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.interactor.RepositorySearchInteractor
import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.ui.activity.RepositorySearchActivity
import concrete.fabio.oliveira.desafioandroid.network.di.module.ApiModule
import dagger.Component

/**
 * Created by Fabio Oliveira
 * Email: fabio91oliveira@gmail.com
 * Mobile: +55 (21) 98191-4951
 * LinkedIn: https://www.linkedin.com/in/fabio91oliveira
 */

@Component(modules = [(RepositorySearchModule::class), (ApiModule::class), (InteractorModule::class)])
interface RepositorySearchComponent {
    fun gson(): Gson
    fun repositorySearchInteractor(): RepositorySearchInteractor
    fun inject(activity: RepositorySearchActivity)
}