package concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.interactor

import concrete.fabio.oliveira.desafioandroid.model.Pull
import io.reactivex.Observable

interface PullsDetailInteractor {
    fun findPulls(creator: String, repository: String): Observable<List<Pull>>
}