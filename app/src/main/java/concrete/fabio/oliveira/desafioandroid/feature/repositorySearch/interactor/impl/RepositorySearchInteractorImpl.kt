package concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.interactor.impl

import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.interactor.RepositorySearchInteractor
import concrete.fabio.oliveira.desafioandroid.model.Repository
import concrete.fabio.oliveira.desafioandroid.network.Api
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RepositorySearchInteractorImpl(private val api: Api): RepositorySearchInteractor {

    override fun findRepository(page: Int): Observable<Repository> {
        return api.findRepository(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

}