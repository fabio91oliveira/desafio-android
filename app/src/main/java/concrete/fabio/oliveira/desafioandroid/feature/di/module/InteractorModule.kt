package concrete.fabio.oliveira.desafioandroid.feature.di.module

import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.interactor.PullsDetailInteractor
import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.interactor.impl.PullsDetailInteractorImpl
import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.interactor.RepositorySearchInteractor
import concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.interactor.impl.RepositorySearchInteractorImpl
import concrete.fabio.oliveira.desafioandroid.network.Api
import dagger.Module
import dagger.Provides

/**
 * Created by Fabio Oliveira
 * Email: fabio91oliveira@gmail.com
 * Mobile: +55 (21) 98191-4951
 * LinkedIn: https://www.linkedin.com/in/fabio91oliveira
 */
@Module
class InteractorModule {

    @Provides
    fun provideRepositorySearchInteractor(api: Api): RepositorySearchInteractor {
        return RepositorySearchInteractorImpl(api)
    }

    @Provides
    fun providePullsDetailInteractor(api: Api): PullsDetailInteractor {
        return PullsDetailInteractorImpl(api)
    }

}