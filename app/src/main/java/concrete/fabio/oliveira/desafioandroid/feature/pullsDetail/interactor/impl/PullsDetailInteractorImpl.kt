package concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.interactor.impl

import concrete.fabio.oliveira.desafioandroid.feature.pullsDetail.interactor.PullsDetailInteractor
import concrete.fabio.oliveira.desafioandroid.model.Pull
import concrete.fabio.oliveira.desafioandroid.network.Api
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PullsDetailInteractorImpl(private val api: Api): PullsDetailInteractor {

    override fun findPulls(creator: String, repository: String): Observable<List<Pull>> {
        return api.findPulls(creator, repository)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}