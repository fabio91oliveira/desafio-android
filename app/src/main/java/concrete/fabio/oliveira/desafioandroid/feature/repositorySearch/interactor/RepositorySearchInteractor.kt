package concrete.fabio.oliveira.desafioandroid.feature.repositorySearch.interactor

import concrete.fabio.oliveira.desafioandroid.model.Repository
import io.reactivex.Observable

interface RepositorySearchInteractor {
    fun findRepository(page: Int): Observable<Repository>
}